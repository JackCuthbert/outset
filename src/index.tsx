import 'css-wipe'
import * as React from 'react'
import ReactDOM from 'react-dom'
import { hot } from 'react-hot-loader'
import {
  ThemeProvider,
  BookmarkProvider,
  CategoryProvider
} from './providers'
import { App } from './components/App'

if (module.hot) module.hot.accept()
const HMR = hot(module)(() => (
  <ThemeProvider>
    <CategoryProvider>
      <BookmarkProvider>
        <App />
      </BookmarkProvider>
    </CategoryProvider>
  </ThemeProvider>
))

ReactDOM.render(<HMR />, document.getElementById('root'))
