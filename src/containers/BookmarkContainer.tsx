import * as React from 'react'
import uuid from 'uuid/v4'
import { styled } from '../styled'
import { AddBookmark, ViewBookmark } from '../components/Bookmark'
import { Category, useCategoryDispatch } from '../providers/CategoryProvider'
import { useBookmarkState, useBookmarkDispatch } from '../providers/BookmarkProvider'
import { Header } from '../styles/Typography'
import { BookmarkGrid } from '../styles/Layout'

interface Props {
  isEditing?: boolean
  category: Category
}

const Container = styled.section`
  margin-bottom: 48px;
`

const HeaderBar = styled.div`
  display: flex;
  align-center: center;
  margin-bottom: 24px;
`

const StyledHeader = styled(Header)`
  flex-grow: 1;
`

function useAddRemoveBookmark () {
  const categoryDispatch = useCategoryDispatch()
  const bookmarkDispatch = useBookmarkDispatch()

  function addBookmark (name: string, url: string, categoryId: string) {
    const newId = uuid()
    bookmarkDispatch({
      type: 'ADD_BOOKMARK',
      id: newId,
      name,
      url
    })
    categoryDispatch({
      type: 'ADD_BOOKMARK_TO_CATGORY',
      categoryId: categoryId,
      bookmarkId: newId
    })
  }

  function delBookmark (bookmarkId: string, categoryId: string) {
    categoryDispatch({
      type: 'DEL_BOOKMARK_FROM_CATGORY',
      bookmarkId,
      categoryId
    })
    bookmarkDispatch({
      type: 'DEL_BOOKMARK',
      id: bookmarkId
    })
  }

  return { addBookmark, delBookmark }
}

export const BookmarkContainer: React.FC<Props> = (props) => {
  const { category, isEditing = false } = props

  const bookmarks = useBookmarkState()
  const { addBookmark, delBookmark } = useAddRemoveBookmark()

  return (
    <Container>
      <HeaderBar>
        <StyledHeader>{category.name}</StyledHeader>
      </HeaderBar>
      <BookmarkGrid>
        {category.bookmarks.map(id => (
          <ViewBookmark
            key={bookmarks[id].id}
            id={bookmarks[id].id}
            name={bookmarks[id].name}
            url={bookmarks[id].url}
            categoryColor={category.color}
            isEditing={isEditing}
            onDelete={id => { delBookmark(id, category.id) }}
          />
        ))}
        {isEditing && (
          <AddBookmark
            categoryColor={category.color}
            onSave={(name, url) => { addBookmark(name, url, category.id) }}
          />
        )}
      </BookmarkGrid>
    </Container>

  )
}
