import { css } from 'styled-components'
import { styled } from '../../styled'
import { CosmeticProps } from './index'

export const Container = styled.div<CosmeticProps>`
  display: flex;
  border-left: 5px solid ${props => props.theme[props.catColor]};
`

const contentStyles = css`
  display: flex;
  flex-direction: column;
  padding: 8px 24px 8px 12px;
`

// Editable variant
export const EditableContainer = styled.div`
  ${contentStyles};
  flex-grow: 1;
  border: 3px dashed ${props => props.theme.base02};
  border-left: 0;
  border-right: 0;
`
export const Editable = styled.input`
  display: block;
  width: 100%;
  color: ${props => props.theme.base07};
  flex-grow: 1;
  font-size: 16px;
  line-height: 1.2;

  &::placeholder {
    color: ${props => props.theme.base04};
  }
`
export const EditableName = styled(Editable)`
  font-size: 18px;
`
export const EditableUrl = styled(Editable)``

// link variant
export const LinkContainer = styled.a`
  ${contentStyles}
  display: block;
  color: ${props => props.theme.base05};
  background-color: ${props => props.theme.base01};
  text-decoration: none;
  flex-grow: 1;
`
export const Name = styled.span`
  display: block;
  clear: right;
  color: ${props => props.theme.base05};
  font-size: 18px;
  flex-grow: 1;
`
export const Url = styled.span`
  display: block;
  color: ${props => props.theme.base0C};
  font-size: 16px;
  flex-grow: 1;
`

type ButtonProps = { additive?: boolean }
export const ActionButton = styled.button.attrs({ type: 'button' })<ButtonProps>`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  min-width: 90px;
  font-size: 18px;
  cursor: pointer;
  color: ${props => props.theme.base00};
  background-color: ${props => props.additive ? props.theme.base0A : props.theme.base08};
`
