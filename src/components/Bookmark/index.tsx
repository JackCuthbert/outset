import * as React from 'react'
import { Theme } from '../../styled'
import {
  ActionButton,
  Container,
  EditableContainer,
  EditableName,
  EditableUrl,
  LinkContainer,
  Name,
  Url
} from './styles'

export interface CosmeticProps {
  categoryColor: keyof Theme
}

interface AddBookmarkProps extends CosmeticProps {
  onSave (name: string, url: string): void
}

export const AddBookmark: React.FC<AddBookmarkProps> = React.memo((props) => {
  const { categoryColor, onSave } = props

  const [name, setName] = React.useState('')
  const [url, setUrl] = React.useState('')

  function handleSave () {
    setName('')
    setUrl('')
    onSave(name, url)
  }

  return (
    <Container categoryColor={categoryColor}>
      <EditableContainer>
        <EditableName
          placeholder='Bookmark name...'
          onChange={e => { setName(e.currentTarget.value) }}
          value={name}
        />
        <EditableUrl
          placeholder='Bookmark URL...'
          onChange={e => { setUrl(e.currentTarget.value) }}
          value={url}
        />
      </EditableContainer>
      <ActionButton additive onClick={handleSave}>
        Save
      </ActionButton>
    </Container>
  )
})

export interface ViewBookmarkProps extends CosmeticProps {
  id: string
  name: string
  url: string
  isEditing?: boolean
  onDelete (id: string): void
}

export const ViewBookmark: React.FC<ViewBookmarkProps> = React.memo((props) => {
  const { categoryColor, id, name, url, isEditing = false } = props

  return (
    <Container categoryColor={categoryColor}>
      <LinkContainer href={url}>
        <Name>{name}</Name>
        <Url>{url}</Url>
      </LinkContainer>
      {isEditing && (
        <ActionButton onClick={() => props.onDelete(id)}>
          Delete
        </ActionButton>
      )}
    </Container>
  )
})
