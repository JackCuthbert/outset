import * as React from 'react'
import { faPaintBrush } from '@fortawesome/free-solid-svg-icons'
import * as themes from '../../theme'
import { styled, Theme } from '../../utils/theme'
import { useThemeContext } from '../../providers/ThemeProvider'
import { Button } from './../Button'
import { Modal } from './../Modal'
import { ButtonGrid } from '../../styles/Layout'

const SetThemeLink = styled.button`
  margin-left: 4px;
  color: ${props => props.theme.base0D};
  cursor: pointer;
`

const ThemeList = styled.ul`
  margin-bottom: 24px;
`

const ThemeName = styled.li`
  margin-bottom: 4px;
  color: ${props => props.theme.base05};
`

const Swatch = styled.div<{ fillColor: string }>`
  text-align: center;
  padding: 10px;
  font-size: 18px;
  color: ${props => props.theme.base00};
  text-shadow: 0 0 3px rgba(240, 240, 240, 0.8);
  background-color: ${props => props.fillColor};
`

const SwatchContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  margin-bottom: 24px;
`

export const EditThemeForm: React.FC = () => {
  const [theme, setTheme] = useThemeContext()

  const [modalIsOpen, setModalIsOpen] = React.useState(false)
  const themeValues = React.useMemo(() => Object.entries(themes), [])
  const themeKeys = React.useMemo(() => Object.keys(theme), [])

  function handleSetTheme (theme: Theme) {
    setTheme(theme)
  }

  return (
    <div>
      <Button
        block
        icon={faPaintBrush}
        onClick={() => { setModalIsOpen(true) }}
      >
        Edit theme
      </Button>

      <Modal title='Edit theme' isOpen={modalIsOpen}>
        <ThemeList>
          {themeValues.map(([name, theme]) => (
            <ThemeName key={`setTheme-${name}`}>
              {name} <SetThemeLink onClick={() => handleSetTheme(theme)}>(Use)</SetThemeLink>
            </ThemeName>
          ))}
        </ThemeList>

        <SwatchContainer>
          {themeKeys.map(key => (
            <Swatch
              key={key}
              fillColor={theme[key as keyof Theme]}
            >
              {key.substr(4, 2)}
            </Swatch>
          ))}
        </SwatchContainer>

        <ButtonGrid>
          <Button onClick={() => { setModalIsOpen(false) }}>
            Done
          </Button>

          <Button onClick={() => { setModalIsOpen(false) }}>
            Cancel
          </Button>
        </ButtonGrid>
      </Modal>
    </div>
  )
}
