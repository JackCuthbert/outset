import * as React from 'react'
import uuid from 'uuid/v4'
import { faPlus } from '@fortawesome/free-solid-svg-icons'

import { useThemeContext } from '../../providers/ThemeProvider'
import { useBookmarkDispatch, Bookmark } from '../../providers/BookmarkProvider'
import { useCategoryDispatch, Category } from '../../providers/CategoryProvider'

import { styled } from '../../utils/theme'
import { ButtonGrid } from '../../styles/Layout'
import { Button } from '../Button'
import { Modal } from '../Modal'

const Input = styled.input.attrs({ type: 'text' })`
  display: block;
  width: 100%;
  background-color: ${props => props.theme.base02};
  color: ${props => props.theme.base07};
  padding: 12px 24px;
  margin-bottom: 12px;
`

export const CreateBookmarkForm: React.FC = () => {
  const [modalIsOpen, setModalIsOpen] = React.useState(false)
  const [theme] = useThemeContext()
  const themeColors = React.useMemo(() => Object.keys(theme), [theme])

  const [name, setName] = React.useState('')
  const [url, setUrl] = React.useState('')
  const [category, setCategory] = React.useState('')
  const [currCatColor, setCurrCatColor] = React.useState<keyof typeof theme>('base00')

  const bookmarkDispatch = useBookmarkDispatch()
  const categoryDispatch = useCategoryDispatch()

  function handleCreateBookmark () {
    const newBookmark: Bookmark = { id: uuid(), name, url }
    bookmarkDispatch({ type: 'ADD_BOOKMARK', ...newBookmark })

    const newCategory: Category = {
      id: uuid(),
      name: category,
      color: currCatColor,
      bookmarks: [newBookmark.id]
    }
    categoryDispatch({ type: 'ADD_CATEGORY', ...newCategory })

    setModalIsOpen(false)
  }

  function handleChangeColor (e: React.ChangeEvent<HTMLSelectElement>) {
    setCurrCatColor(e.currentTarget.value as any)
  }

  function openModal () {
    setModalIsOpen(true)
  }

  function closeModal () {
    setModalIsOpen(false)
  }

  return (
    <div>
      <Button block icon={faPlus} onClick={openModal}>
        New bookmark
      </Button>

      <Modal title='New bookmark' isOpen={modalIsOpen}>
        <Input
          value={name}
          onChange={e => setName(e.currentTarget.value)}
          placeholder='Bookmark name'
        />
        <Input
          value={url}
          onChange={e => setUrl(e.currentTarget.value)}
          placeholder='Bookmark url'
        />

        <Input
          value={category}
          onChange={e => setCategory(e.currentTarget.value)}
          placeholder='Category'
        />

        <select onChange={handleChangeColor}>
          {themeColors.map((key) => (
            <option key={key} value={key}>{key}</option>
          ))}
        </select>

        <ButtonGrid>
          <Button onClick={handleCreateBookmark}>
            Add
          </Button>
          <Button plain onClick={closeModal}>
            Cancel
          </Button>
        </ButtonGrid>
      </Modal>
    </div>
  )
}
