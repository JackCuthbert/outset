import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { styled } from '../styled'

interface OwnProps {
  icon?: typeof faPlus
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

interface CosmeticProps {
  isActive?: boolean
  disabled?: boolean
  block?: boolean
}

const Icon = styled(FontAwesomeIcon)`
  margin-right: 12px;
`

const StyledButton = styled.button.attrs({ type: 'button' })<CosmeticProps>`
  display: inline-flex;
  width: ${props => props.block ? '100%' : 'auto'};
  align-items: center;
  height: 48px;
  font-size: 18px;
  padding: 0 24px;
  cursor: pointer;
  transition: box-shadow 50ms ease-in-out;

  box-shadow: inset 0 0 0 1px ${props => props.isActive
    ? props.theme.base0A
    : props.theme.base02
  };
  color: ${props => props.isActive
    ? props.theme.base0A
    : props.theme.base05
  };

  &::-moz-focus-inner {
    border: 0;
  }

  &:hover, &:active {
    outline: none;
    box-shadow: inset 0 0 0 2px ${props => props.isActive
      ? props.theme.base0A
      : props.theme.base02
    };
  }
`

type Props = OwnProps & CosmeticProps

const ButtonComponent: React.FC<Props> = ({ children, icon, ...rest }) => {
  return (
    <StyledButton {...rest}>
      {icon !== undefined && <Icon icon={icon} size='sm'/>}
      {children}
    </StyledButton>
  )
}

export const Button = styled(ButtonComponent)``
