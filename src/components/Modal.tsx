import * as React from 'react'
import { hot } from 'react-hot-loader'
import { ThemeContext } from 'styled-components'
import { default as ReactModal } from 'react-modal'
import { styled, Theme } from '../utils/theme'
import { hexToRgb } from '../utils/hexToRgb'

interface Props {
  isOpen?: boolean
  title: string
  onClose?: () => void
}

ReactModal.setAppElement('#modalTarget')

const StyledModal = styled(ReactModal)`
  padding: 24px;
`

const Header = styled.h2`
  display: block;
  text-align: center;
  color: ${props => props.theme.base05};
  margin-bottom: 24px;
`

const Modal: React.FC<Props> = ({ title, children, isOpen }) => {
  const theme = React.useContext<Theme>(ThemeContext)
  const [bgR, bgG, bgB] = hexToRgb(theme.base01)

  return (
    <StyledModal
      isOpen={isOpen || false}
      contentLabel={title}
      style={{
        overlay: {
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: `rgba(${bgR}, ${bgG}, ${bgB}, 0.9)`
        },
        content: {
          minWidth: '512px',
          minHeight: '512px',
          backgroundColor: theme.base00,
          boxShadow: `0 10px 36px rgba(0, 0, 0, 0.15)`
        }
      }}
    >
      <Header>{title}</Header>
      {children}
    </StyledModal>
  )
}

if (module.hot) {
  module.hot.accept()
}
const hotModal = hot(module)(Modal)
export { hotModal as Modal }
