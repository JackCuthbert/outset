import * as React from 'react'
import uuid from 'uuid/v4'
import { faPlus, faEdit, faFileImport, faCheck } from '@fortawesome/free-solid-svg-icons'
import { styled } from '../styled'
import { GlobalStyle } from '../styles/GlobalStyle'
import { Header } from '../styles/Typography'
import {
  AppWrap,
  Hr,
  Container,
  SettingsGrid
} from '../styles/Layout'
import { Button } from './Button'
import { EditThemeForm } from '../components/forms/EditThemeForm'
import { BookmarkContainer } from '../containers/BookmarkContainer'
import { useCategoryState, useCategoryDispatch } from '../providers/CategoryProvider'

const EditableCategory = styled.input.attrs({ type: 'text' })`
  color: ${props => props.theme.base07};
  font-family: 'Heebo', sans-serif;
  font-size: 18px;

  &::placeholder {
    color: ${props => props.theme.base04};
  }
`

export const App: React.FC = () => {
  const categories = useCategoryState()
  const categoryDispatch = useCategoryDispatch()
  const [isEditing, setIsEditing] = React.useState(false)

  const categoryList = React.useMemo(() => Object.values(categories), [categories])

  return (
    <AppWrap>
      <GlobalStyle />
      <Container>
        {categoryList.length > 0 && categoryList.map(category => (
          <BookmarkContainer
            key={category.id}
            category={category}
            isEditing={isEditing}
          />
        ))}

        {isEditing && (
          // TODO: CategoryContainer
          <div style={{ marginBottom: '48px' }}>
            <EditableCategory placeholder='Category name...' />
            <Button icon={faPlus} onClick={() => {
              categoryDispatch({
                type: 'ADD_CATEGORY',
                id: uuid(),
                name: 'New category',
                color: 'base0D',
                bookmarks: []
              })
            }}>
              Add category
            </Button>
          </div>
        )}

        <Hr />

        {/* TODO: Settings container */}
        <Header>Configure</Header>
          <SettingsGrid>
            {/* <CreateBookmarkForm /> */}
            <Button
              block
              icon={isEditing ? faCheck : faEdit}
              isActive={isEditing}
              onClick={() => { setIsEditing(!isEditing) }}
            >
              {isEditing ? 'Done editing' : 'Edit bookmarks'}
            </Button>
            <EditThemeForm />
            <Button block disabled icon={faFileImport}>
              Import
            </Button>
          </SettingsGrid>
      </Container>
    </AppWrap>
  )
}
