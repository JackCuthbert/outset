import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  body {
    font-family: 'Muli', sans-serif;
    font-weight: 400;
    line-height: 1.5;
  }

  h1, h2, h3, h4, h5, h6 {
    font-family: 'Heebo', sans-serif;
    font-weight: 900;
    line-height: 1.5;
  }

  button, input {
    border: 0;
    background: transparent;
    padding: 0;
    margin: 0;

    &:hover, &:active {
      outline: none;
    }
    &::-moz-focus-inner {
      border: 0;
    }
  }
`
