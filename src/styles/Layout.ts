import { styled } from '../utils/theme'

export const AppWrap = styled.div`
  min-height: 100vh;
  background-color: ${props => props.theme.base00};
  color: ${props => props.theme.base05};
`

export const Container = styled.div`
  max-width: 1024px;
  margin: 0 auto;
  padding: 36px 24px 0 24px;
`

export const CategoryContainer = styled.section`
  margin-bottom: 48px;
`

export const BookmarkGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 24px;
`

export const SettingsGrid = styled(BookmarkGrid)`
  grid-template-columns: repeat(4, 1fr);
`

export const ButtonGrid = styled.div`
  display: inline-flex;
`

export const Hr = styled.hr`
  border-top: 1px solid ${props => props.theme.base02}
  margin: 0 0 36px 0;
`
