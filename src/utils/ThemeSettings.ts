import { Theme } from '../styled'
import { dracula } from '../theme'

export class ThemeSettings {
  /**
   * Parse a string into a Theme object. Throws if the string is invalid.
   */
  public static parseThemeString (str: string): Theme {
    const re = /^(?:#[a-f0-9].{5}[,\s]?\s?){16}$/gi

    if (!re.test(str)) {
      throw Error('Invalid theme string')
    }

    const split = str.split(',')
    const newTheme = this.newThemeObj()

    return Object.keys(newTheme).reduce<Theme>((theme, key, idx) => {
      theme[key as keyof Theme] = split[idx].trim().toUpperCase()
      return theme
    }, newTheme)
  }

  /**
   * Turn a Theme object into a valid theme string
   */
  public static stringifyTheme (theme: Theme): string {
    return Object.values(theme).join(',')
  }

  /**
   * Load a theme from local storage
   */
  public static loadTheme (key = 'outset.theme'): Theme {
    const themeString = localStorage.getItem(key)
    if (themeString === null) return dracula
    return this.parseThemeString(themeString)
  }

  /**
   * Stringify a theme to localStorage
   */
  public static saveTheme (theme: Theme, key = 'outset.theme'): void {
    localStorage.setItem(key, this.stringifyTheme(theme))
  }

  /**
   * Return a blank Theme object
   */
  private static newThemeObj (): Theme {
    return {
      base00: '',
      base01: '',
      base02: '',
      base03: '',
      base04: '',
      base05: '',
      base06: '',
      base07: '',
      base08: '',
      base09: '',
      base0A: '',
      base0B: '',
      base0C: '',
      base0D: '',
      base0E: '',
      base0F: ''
    }
  }
}
