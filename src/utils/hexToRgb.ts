/**
 * Convert hex colour codes to rgb
 */
export function hexToRgb (hex: string): [number, number, number] {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  if (!result) throw Error(`Unable to parse "${hex}" to RGB`)
  return [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)]
}
