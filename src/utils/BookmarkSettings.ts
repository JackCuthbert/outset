import { Theme } from '../utils/theme'

export interface Bookmark {
  id: string
  name: string
  url: string
}

export interface Category {
  id: string
  color: keyof Theme,
  bookmarks: string[]
  name: string
}

export type CategoryList = { [categoryId: string]: Category }
export type BookmarkList = { [bookmarkId: string]: Bookmark }

export class BookmarkSettings {
  public static saveBookmarks (categories: CategoryList, bookmarks: BookmarkList): void {
    localStorage.setItem('outset.categories', JSON.stringify(categories))
    localStorage.setItem('outset.bookmarks', JSON.stringify(bookmarks))
  }

  public static loadBookmarks (): [CategoryList, BookmarkList] {
    const defaultCategory: CategoryList = {
      1: { id: '1', name: 'Top', color: 'base08', bookmarks: ['1', '4', '5'] },
      2: { id: '2', name: 'Sample Category', color: 'base0F', bookmarks: ['2', '6'] },
      3: { id: '3', name: 'Sample Category Two', color: 'base0D', bookmarks: ['3'] }
    }

    const defaultBookmarks: BookmarkList = {
      1: {
        id: '1',
        name: 'Reddit',
        url: 'https://reddit.com'
      },
      2: {
        id: '2',
        name: 'YouTube',
        url: 'https://youtube.com'
      },
      3: {
        id: '3',
        name: 'HackerNews',
        url: 'https://news.ycombinator.com'
      },
      4: {
        id: '4',
        name: 'r/MechanicalKeyboards',
        url: 'https://reddit.com/r/MechanicalKeyboards'
      },
      5: {
        id: '5',
        name: 'GitHub',
        url: 'https://github.com'
      },
      6: {
        id: '6',
        name: 'Stack Overflow',
        url: 'https://stackoverflow.com'
      }
    }

    const categoriesStr = localStorage.getItem('outset.categories')
    const bookmarksStr = localStorage.getItem('outset.bookmarks')

    return [
      categoriesStr === null ? defaultCategory : JSON.parse(categoriesStr),
      bookmarksStr === null ? defaultBookmarks : JSON.parse(bookmarksStr)
    ]
  }
}
