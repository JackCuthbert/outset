import { ThemeSettings } from '../ThemeSettings'

describe('ThemeSettings', () => {
  const THEME_STORAGE_KEY = 'outset.theme'
  const THEME_OBJECT = {
    base00: '#282936',
    base01: '#3A3C4E',
    base02: '#4D4F68',
    base03: '#626483',
    base04: '#62D6E8',
    base05: '#E9E9F4',
    base06: '#F1F2F8',
    base07: '#F7F7FB',
    base08: '#EA51B2',
    base09: '#B45BCF',
    base0A: '#00F769',
    base0B: '#EBFF87',
    base0C: '#A1EFE4',
    base0D: '#62D6E8',
    base0E: '#B45BCF',
    base0F: '#00F769'
  }

  describe('#parseThemeString', () => {
    it('parses theme strings into Theme objects', () => {
      const themeStrings = [
        '#282936, #3a3c4e, #4d4f68, #626483, #62d6e8, #e9e9f4, #f1f2f8, #f7f7fb, #ea51b2, #b45bcf, #00f769, #ebff87, #a1efe4, #62d6e8, #b45bcf, #00f769',
        '#282936,#3a3c4e,#4d4f68,#626483,#62d6e8,#e9e9f4,#f1f2f8,#f7f7fb,#ea51b2,#b45bcf,#00f769,#ebff87,#a1efe4,#62d6e8,#b45bcf,#00f769',
        '#282936, #3A3C4E, #4D4F68, #626483, #62D6E8, #E9E9F4, #F1F2F8, #F7F7FB, #EA51B2, #B45BCF, #00F769, #EBFF87, #A1EFE4, #62D6E8, #B45BCF, #00F769',
        '#282936,#3A3C4E,#4D4F68,#626483,#62D6E8,#E9E9F4,#F1F2F8,#F7F7FB,#EA51B2,#B45BCF,#00F769,#EBFF87,#A1EFE4,#62D6E8,#B45BCF,#00F769'
      ]

      expect.assertions(themeStrings.length)
      for (const str of themeStrings) {
        expect(ThemeSettings.parseThemeString(str)).toEqual(THEME_OBJECT)
      }
    })

    it('throws when theme string is invalid', () => {
      expect(() => {
        ThemeSettings.parseThemeString('nope')
      }).toThrow()
      expect(() => {
        ThemeSettings.parseThemeString('#123456,#123456,#123456,#123456,#123456,#123456')
      }).toThrow()
      expect(() => {
        ThemeSettings.parseThemeString('aksjdnfiausdnf0978241pun4 ;alsk d;kjasndfkl asjdbfp8as7dgfb;1k;1 j')
      }).toThrow()
    })
  })

  describe('#stringifyTheme', () => {
    it('turns a theme object into a valid theme string', () => {
      const result = ThemeSettings.stringifyTheme(THEME_OBJECT)
      expect(ThemeSettings.parseThemeString(result)).toEqual(THEME_OBJECT)
    })
  })

  describe('#loadTheme', () => {
    beforeEach(() => {
      localStorage.clear()
    })

    it('loads the default theme string from localStorage', () => {
      const result = ThemeSettings.loadTheme(THEME_STORAGE_KEY)
      expect(localStorage.getItem).toHaveBeenLastCalledWith(THEME_STORAGE_KEY)
      expect(result).toEqual(THEME_OBJECT)
    })

    it('loads an existing theme string from localStorage', () => {
      const savedTheme = '#000000,#3A3C4E,#4D4F68,#626483,#62D6E8,#E9E9F4,#F1F2F8,#F7F7FB,#EA51B2,#B45BCF,#00F769,#EBFF87,#A1EFE4,#62D6E8,#B45BCF,#00F769'
      localStorage.setItem(THEME_STORAGE_KEY, savedTheme)

      const result = ThemeSettings.loadTheme(THEME_STORAGE_KEY)
      expect(result).toEqual({ ...THEME_OBJECT, base00: '#000000' })
    })
  })

  describe('#saveTheme', () => {
    beforeEach(() => {
      localStorage.clear()
    })

    it('saves a valid theme string to localStorage', () => {
      ThemeSettings.saveTheme(THEME_OBJECT, THEME_STORAGE_KEY)
      const expected = ThemeSettings.stringifyTheme(THEME_OBJECT)

      expect(localStorage.setItem).toHaveBeenLastCalledWith(THEME_STORAGE_KEY, expected)
    })
  })
})
