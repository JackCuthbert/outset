import * as React from 'react'
import {
  ThemeProvider as StyledThemeProvider,
  ThemeContext
} from 'styled-components'
import { Theme, defaultTheme } from '../utils/theme'
import { ThemeSettings } from '../utils/ThemeSettings'

type SetThemeAction = React.Dispatch<React.SetStateAction<Theme>>
export const SetThemeContext = React.createContext<SetThemeAction | undefined>(undefined)

/**
 * Main `styled-components` theme provider wrapper. This component manages which
 * theme is active and the saving/loading of said theme.
 */
const ThemeProvider: React.FC = ({ children }) => {
  const [theme, setTheme] = React.useState(defaultTheme)
  const [themeLoaded, setThemeLoaded] = React.useState(false)

  // don't run on first load, run every `theme` change
  const initialRender = React.useRef(true)
  React.useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false
      return
    }

    if (themeLoaded) {
      ThemeSettings.saveTheme(theme)
    }
  }, [theme])

  // run once, on load
  React.useEffect(() => {
    const theme = ThemeSettings.loadTheme()
    setTheme(theme)
    setThemeLoaded(true)
  }, [])

  // return a nice loading state if no theme has been loaded yet
  if (!themeLoaded) {
    return <p>Loading</p>
  }

  return (
    <StyledThemeProvider theme={theme}>
      <SetThemeContext.Provider value={setTheme}>
        {children}
      </SetThemeContext.Provider>
    </StyledThemeProvider>
  )
}

/** Hook that provides the current theme and a function to update it */
function useThemeContext (): [Theme, SetThemeAction] {
  const theme = React.useContext<Theme>(ThemeContext)
  const setTheme = React.useContext(SetThemeContext)
  if (setTheme === undefined) {
    throw Error('useThemeContext must be used within a ThemeProvider')
  }
  return [theme, setTheme]
}

export { ThemeProvider, useThemeContext }
