import * as React from 'react'
import { Theme } from '../styled'

export interface Category {
  /** UUIDv4 */
  id: string
  name: string
  color: keyof Theme
  bookmarks: string[]
}

export interface State {
  [categoryId: string]: Category
}

type AddCategoryAction = { type: 'ADD_CATEGORY' } & Category
type DelCategoryAction = { type: 'DEL_CATEGORY', id: string }
type LoadCategoriesAction = { type: 'LOAD_CATEGORIES', categories: State }
type AddBookmarkToCategoryAction = {
  type: 'ADD_BOOKMARK_TO_CATGORY',
  bookmarkId: string,
  categoryId: string
}
type DelBookmarkFromCategoryAction = {
  type: 'DEL_BOOKMARK_FROM_CATGORY',
  bookmarkId: string,
  categoryId: string
}

type Action =
  | AddCategoryAction
  | DelCategoryAction
  | LoadCategoriesAction
  | AddBookmarkToCategoryAction
  | DelBookmarkFromCategoryAction

const initialState: State = {}
const init = () => ({ ...initialState })

function reducer (state: State, action: Action): State {
  switch (action.type) {
    case 'ADD_CATEGORY': {
      const newCat: Category = {
        id: action.id,
        name: action.name,
        color: action.color,
        bookmarks: action.bookmarks
      }
      return { ...state, [newCat.id]: newCat }
    }
    case 'DEL_CATEGORY': {
      return Object.keys(state)
        .filter(id => id !== action.id)
        .reduce<State>((cats, id) => {
          cats[id] = state[id]
          return cats
        }, {})
    }
    case 'ADD_BOOKMARK_TO_CATGORY': {
      const editing = { ...state[action.categoryId] }
      editing.bookmarks.push(action.bookmarkId)
      return { ...state, [editing.id]: editing }
    }
    case 'DEL_BOOKMARK_FROM_CATGORY': {
      const editing = { ...state[action.categoryId] }
      editing.bookmarks = editing.bookmarks
        .filter(id => id !== action.bookmarkId)
      return { ...state, [editing.id]: editing }
    }
    case 'LOAD_CATEGORIES': {
      return action.categories
    }
    default:
      return state
  }
}

const StateContext = React.createContext<State | undefined>(undefined)
const DispatchContext = React.createContext<React.Dispatch<Action> | undefined>(undefined)

const CategoryProvider: React.FC = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState, init)

  // run once to load any categories in localStorage
  React.useEffect(() => {
    const saved = localStorage.getItem('outset.categories')
    if (saved === null) return

    console.debug('loading categories from localStorage...')
    const categories = JSON.parse(saved) as State
    dispatch({ type: 'LOAD_CATEGORIES', categories })
  }, [])

  // persist categories to localStorage when they change
  React.useEffect(() => {
    if (Object.keys(state).length === 0) return

    console.debug('persisting categories to localStorage...')
    localStorage.setItem('outset.categories', JSON.stringify(state))
  }, [state])

  return (
    <StateContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        {children}
      </DispatchContext.Provider>
    </StateContext.Provider>
  )
}

/** Hook that provides access to the Category state */
function useCategoryState () {
  const context = React.useContext(StateContext)
  if (context === undefined) {
    throw Error('useCategoryState must be used within a CategoryProvider')
  }
  return context
}

/** Hook that provides access to the Category state dispatcher */
function useCategoryDispatch () {
  const context = React.useContext(DispatchContext)
  if (context === undefined) {
    throw Error('useCategoryDispatch must be used within a CategoryProvider')
  }
  return context
}

export { CategoryProvider, useCategoryState, useCategoryDispatch }
