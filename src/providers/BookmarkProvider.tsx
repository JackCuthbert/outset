import * as React from 'react'

export interface Bookmark {
  /** UUIDv4 */
  id: string
  name: string
  url: string
}

export interface State {
  [bookmarkId: string]: Bookmark
}

type AddBookmarkAction = { type: 'ADD_BOOKMARK' } & Bookmark
type DelBookmarkAction = { type: 'DEL_BOOKMARK', id: string }
type LoadBookmarksAction = { type: 'LOAD_BOOKMARKS', bookmarks: State }
type Action =
  | AddBookmarkAction
  | DelBookmarkAction
  | LoadBookmarksAction

const initialState: State = {}
const init = () => ({ ...initialState })

function reducer (state: State, action: Action): State {
  switch (action.type) {
    case 'ADD_BOOKMARK': {
      const newBmrk: Bookmark = {
        id: action.id,
        name: action.name,
        url: action.url
      }
      return { ...state, [newBmrk.id]: newBmrk }
    }
    case 'DEL_BOOKMARK': {
      return Object.keys(state)
        .filter(id => id !== action.id)
        .reduce<State>((bmrks, id) => {
          bmrks[id] = state[id]
          return bmrks
        }, {})
    }
    case 'LOAD_BOOKMARKS': {
      return action.bookmarks
    }
    default:
      return state
  }
}

const StateContext = React.createContext<State | undefined>(undefined)
const DispatchContext = React.createContext<React.Dispatch<Action> | undefined>(undefined)

/** Provide access to the global Bookmark list */
const BookmarkProvider: React.FC = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState, init)

  // run once to load any bookmarks in localStorage
  React.useEffect(() => {
    const saved = localStorage.getItem('outset.bookmarks')
    if (saved === null) return

    console.debug('loading bookmarks from localStorage...')
    const bookmarks = JSON.parse(saved) as State
    dispatch({ type: 'LOAD_BOOKMARKS', bookmarks })
  }, [])

  // persist bookmarks to localStorage when they change
  React.useEffect(() => {
    if (Object.keys(state).length === 0) return

    console.debug('persisting bookmarks to localStorage...')
    localStorage.setItem('outset.bookmarks', JSON.stringify(state))
  }, [state])

  return (
    <StateContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        {children}
      </DispatchContext.Provider>
    </StateContext.Provider>
  )
}

/** Hook that provides access to the Bookmark list */
function useBookmarkState () {
  const state = React.useContext(StateContext)

  if (state === undefined) {
    throw Error('useBookmarkState must be used within a BookmarkProvider')
  }
  return state
}

/** Hook that provides access to the Bookmark dispatcher function */
function useBookmarkDispatch () {
  const dispatch = React.useContext(DispatchContext)

  if (dispatch === undefined) {
    throw Error('useBookmarkState must be used within a BookmarkProvider')
  }
  return dispatch
}

export { useBookmarkState, useBookmarkDispatch, BookmarkProvider }
